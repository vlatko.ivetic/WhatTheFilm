var admin = require("firebase-admin");

var serviceAccount = require("./firebaseServiceAccount.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://what-the-film-fa6dd.firebaseio.com"
});

var db = admin.firestore();

var movieRef = db.collection('movies');
var genreRef = db.collection('genres');
var emotionRef = db.collection('emotions');
var userRef = db.collection('users');


var theMovieDb = require('./tmdb_API.js')

function successCB(data) {
  console.log("Success callback: " + data);
};

function errorCB(data) {
  console.log("Error callback: " + data);
};

/* 
db.collection('emotions').doc('love').set({
  unicode : ""
}).then(ref => {
  console.log('Added document with ID: ', ref.id);
});
db.collection('emotions').doc('sad').set({
  unicode : ""
}).then(ref => {
  console.log('Added document with ID: ', ref.id);
});
db.collection('emotions').doc('confused').set({
  unicode : ""
}).then(ref => {
  console.log('Added document with ID: ', ref.id);
});
db.collection('emotions').doc('hilarious').set({
  unicode : ""
}).then(ref => {
  console.log('Added document with ID: ', ref.id);
});
*/



/*
theMovieDb.genres.getMovieList({}, function (data) {
  var genres = JSON.parse(data).genres;
  genres.forEach(element => {
    console.log(element)
    genreRef.doc('genre_' + element.id).set({
      'id': element.id,
      'name' : element.name
    });
  })
}, errorCB)
*/


const emotion_names = ['angry', 'confused', 'happy', 'hilarious', 'love', 'lovely', 'sad'];

/*
var curr_page = 1;
var total_pages = 1000;
theMovieDb.discover.getMovies({}, function (data) {
  total_pages = JSON.parse(data).total_pages;
  var interval = setInterval(function () {
    if (curr_page <= total_pages) {
      theMovieDb.discover.getMovies({"page":curr_page++}, function (data) {
        var movies = JSON.parse(data).results;
        movies.forEach(element => {
          console.log(element.title)
          movieRef.doc('movie_' + element.id).set({
            'vote_count': 0,
            'id': element.id,
            'video': false,
            'vote_average': 0.0,
            'title': element.title,
            'poster_path': element.poster_path,
            'original_language': element.original_language,
            'original_title': element.original_title,
            'genre_ids': element.genre_ids,
            'backdrop_path': element.backdrop_path,
            'adult': element.adult,
            'overview': element.overview,
            'release_date': element.release_date,
            'emotion_ids' : [emotion_names[Math.floor(Math.random() * emotion_names.length)]],
            'emotions_count' : {
              'happy' : Math.floor(Math.random() * 100),
              'love' : Math.floor(Math.random() * 100),
              'angry' : Math.floor(Math.random() * 100),
              'sad' : Math.floor(Math.random() * 100),
              'confused' : Math.floor(Math.random() * 100),
              'hilarious' : Math.floor(Math.random() * 100),
              'lovely' : Math.floor(Math.random() * 100)
            }
          });
        })
      }, errorCB)
    } else {
      clearInterval(interval);
    }
  }, 80);
}, errorCB);
*/


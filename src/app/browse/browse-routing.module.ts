import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { MoviesGenreComponent } from "./../movies-genre/movies-genre.component";

import { BrowseComponent } from "./browse.component";

const routes: Routes = [
    { path: "default", component: BrowseComponent },
    { path: "genre/:id", component: MoviesGenreComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class BrowseRoutingModule { }

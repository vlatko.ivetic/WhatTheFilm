import { Component, OnInit } from "@angular/core";
import { Genre, GenreService } from "./../shared/genre.service";
import { ListPicker } from "tns-core-modules/ui/list-picker";
import {MovieDetailComponent} from "./../home/movie-detail/movie-detail.component"
import { ItemEventData } from "tns-core-modules/ui/list-view/list-view";
import { Movie, MovieService } from "../shared/movie.service";


@Component({
    selector: "Browse",
    moduleId: module.id,
    templateUrl: "./browse.component.html",
    providers: [MovieService, GenreService],
    styleUrls: ["./../home/movie-detail/movie-detail.component.scss"]
})

export class BrowseComponent implements OnInit {
    
    genres: Array<Genre> = [];
    indexSelected: number = 0;
    public selectedGenre;

    constructor(
        private _genreService: GenreService,
    ) { }

    ngOnInit(): void {
        this._genreService.getGenres().subscribe(loadedGenres => {
            loadedGenres.forEach((genreObject) => {
                this.genres.unshift(genreObject);
            });
            this.selectedGenre = this.genres[this.indexSelected].name
            //console.log(this.genres[0].name);
        }); 
    }

    onItemTap(args: ItemEventData): void {
        this.selectedGenre = this.genres[args.index].name;
        //console.log(this.selectedGenre)
    }



}

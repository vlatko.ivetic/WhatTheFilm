import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { DropDownModule } from "nativescript-drop-down/angular";
import { MoviesGenreComponent } from "./../movies-genre/movies-genre.component";


import { BrowseRoutingModule } from "./browse-routing.module";
import { BrowseComponent } from "./browse.component";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        BrowseRoutingModule,
        DropDownModule
    ],
    declarations: [
        BrowseComponent,
        MoviesGenreComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class BrowseModule { }

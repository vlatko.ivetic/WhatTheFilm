import { Injectable, NgZone } from "@angular/core";
import { Observable, Subscriber } from "rxjs";
import { collectExternalReferences } from "@angular/compiler";

const firebase = require("nativescript-plugin-firebase");
const theMovieDb = require("./../tmdb_API.js");

export interface Movie {
    adult : false;
    backdrop_path : string;
    emotion_ids: [],
    emotions_count: {
        angry: number,
        confused: number,
        happy: number,
        hilarious: number,
        love: number,
        lovely: number,
        sad: number,
    };
    genre_ids: [];
    id: number;
    original_language: string;
    original_title: string;
    overview: string;
    poster_path: string;
    release_date: Date;
    title: string;
    video: string;
    vote_average: number;
    vote_count: number;
}

@Injectable({
    providedIn: "root"
})
export class MovieService {

    constructor(private zone: NgZone) { }

    movies : Array<Movie> = [];
    moviesGenre : Array<Movie> = []
    similarMovies : Array<Movie> = []
    movie : Movie;

    makeMovieFromTMDB(movie_JSON): Movie {
        var similarMovie: Movie = {
            adult: movie_JSON.adult,
            backdrop_path: movie_JSON.backdrop_path,
            emotion_ids: [],
            emotions_count: {
                angry: 0,
                confused: 0,
                happy: 0,
                hilarious: 0,
                love: 0,
                lovely: 0,
                sad: 0
            },
            genre_ids: movie_JSON.genre_ids,
            id: movie_JSON.id,
            original_language: movie_JSON.original_language,
            original_title: movie_JSON.original_title,
            overview: movie_JSON.overview,
            poster_path: movie_JSON.poster_path,
            release_date: movie_JSON.release_date,
            title: movie_JSON.title,
            video: movie_JSON.video,
            vote_average: movie_JSON.vote_average,
            vote_count: movie_JSON.vote_count
        }
        return similarMovie;
    }

    getMovies() : Observable<Array<Movie>> {
        return Observable.create(subscriber => {
            const colRef = firebase.firestore.collection("movies").limit(20);
            colRef.onSnapshot((snapshot) => {
                this.zone.run(() => {
                    this.movies = [];
                    snapshot.forEach(docSnap => { 
                        this.movies.push(<Movie>docSnap.data())
                    });
                    subscriber.next(this.movies);
                });
            });
        });
    }

    getSimilarMovies(id) : Observable<Array<Movie>> { 
        return Observable.create(subscriber => {
            theMovieDb.movies.getSimilarMovies({id:id, page:1}, data => {
                this.zone.run(() => {
                    this.similarMovies = [];
                    JSON.parse(data).results.forEach(similar => {
                        this.similarMovies.push(this.makeMovieFromTMDB(similar));
                    });;
                    subscriber.next(this.similarMovies);
                })
            }, error => console.log(error));
        });
    }

    getMovieByID(id: number) : Observable<Movie> {  
        return Observable.create(subscriber => {
            const docRef = firebase.firestore.collection("movies").doc(`movie_${id}`);
            docRef.onSnapshot((doc) => {
                this.zone.run(() => {
                    this.movie = <Movie>doc.data();
                    subscriber.next(this.movie);
                });
            });
        });
    }

    getMoviesByGenre(id:number) : Observable<Array<Movie>> { 
        return Observable.create(subscriber => {
            const colRef = firebase.firestore.collection("movies").where('genre_ids', 'array-contains', id);
            colRef.onSnapshot((snapshot) => {
                this.zone.run(() => {
                    this.moviesGenre = [];
                    snapshot.forEach(docSnap => { 
                        //console.log(docSnap.data())
                        this.moviesGenre.push(<Movie>docSnap.data())
                    });
                    subscriber.next(this.moviesGenre);
                });
            });
        });
    }

    getMoviesByEmotion(emotion: string) : Observable<Array<Movie>> { 
        return Observable.create(subscriber => {
            const colRef = firebase.firestore.collection("movies").where('emotion_ids', 'array-contains', emotion);
            colRef.onSnapshot((snapshot) => {
                this.zone.run(() => {
                    this.moviesGenre = [];
                    snapshot.forEach(docSnap => { 
                        //console.log(docSnap.data())
                        this.moviesGenre.push(<Movie>docSnap.data())
                    });
                    subscriber.next(this.moviesGenre);
                });
            });
        });
    }
}
import { ImageSource, fromNativeSource, fromFile } from "tns-core-modules/image-source/image-source";
import * as fileSystemModule from "tns-core-modules/file-system";
import { Injectable, NgZone } from "@angular/core";
import { Observable } from "rxjs";
import { Cache } from "tns-core-modules/ui/image-cache";

import { Movie } from "../movie.service";

const firebase = require("nativescript-plugin-firebase");
const theMovieDb = require("./../../tmdb_API.js");

export interface Image {
    source : ImageSource,
    movie_id : number
}

@Injectable({
    providedIn: "root"
})
export class ImageService {
    
    constructor(private zone: NgZone) {
        this.cache.maxRequests = 10;
        //const folder = fileSystemModule.knownFolders.currentApp();
        //const path = fileSystemModule.path.join(folder.path, "images/wtf.png");
        //const imageFromLocalFile = fromFile(path);
    }
    
    cache = new Cache();
    image : Image;
    
    getMovieImage(movie: Movie) : Observable<Image> {
        return Observable.create(subscriber => {
            this.cache.enableDownload();

            let path = movie.poster_path;
            let image = this.cache.get(path);
            if (image) {
                this.image = {
                    source: fromNativeSource(image),
                    movie_id: movie.id
                };
                subscriber.next(this.image)
            } else {
                this.cache.enqueue({ 
                    key: path,
                    url: theMovieDb.common.images_uri + "w500" + "/" + path,
                    completed: (image, key) => {
                        this.image = {
                            source: fromNativeSource(image),
                            movie_id: movie.id
                        };
                        subscriber.next(this.image);
                    }
                });
            }
        });
    }
}
import { Injectable, NgZone } from "@angular/core";
import { Observable } from "rxjs";

const firebase = require("nativescript-plugin-firebase");

export interface Emotion {
    unicode : string
}

@Injectable({
    providedIn: "root"
})
export class EmotionService {
    
    constructor(private zone: NgZone) { }

    emotions : Array<Emotion> = []
    emotion : Emotion;

    getEmotions() : Observable<Array<Emotion>> {
        return Observable.create(subscriber => {
            const colRef = firebase.firestore.collection("emotions");
            colRef.onSnapshot((snapshot) => {
                this.zone.run(() => {
                    this.emotions = [];
                    snapshot.forEach(docSnap => { 
                        this.emotions.push(<Emotion>docSnap.data())
                    });
                    subscriber.next(this.emotions);
                });
            });
        });
    }

    getEmotionUnicode(id: string) {
        return Observable.create(subscriber => {
            const docRef = firebase.firestore.collection("emotions").doc(id);
            docRef.onSnapshot((doc) => {
                this.zone.run(() => {
                    this.emotion = <Emotion>doc.data();
                    subscriber.next(this.emotion);
                });
            });
        });
    }
}
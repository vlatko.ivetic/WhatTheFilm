import { Injectable, NgZone } from "@angular/core";
import { Observable } from "rxjs";

const firebase = require("nativescript-plugin-firebase");

export interface Genre {
    id : number,
    name : string
}

@Injectable({
    providedIn: "root"
})
export class GenreService {

    constructor(private zone: NgZone) { }

    genres : Array<Genre> = []
    genre : Genre;

    getGenres() : Observable<Array<Genre>> {
        return Observable.create(subscriber => {
            const colRef = firebase.firestore.collection("genres");
            colRef.onSnapshot((snapshot) => {
                this.zone.run(() => {
                    this.genres = [];
                    snapshot.forEach(docSnap => { 
                        this.genres.push(<Genre>docSnap.data())
                    });
                    subscriber.next(this.genres);
                });
            });
        });
    }
    
    getGenreByID(id: number) {
        return Observable.create(subscriber => {
            const docRef = firebase.firestore.collection("genres").doc(`genre_${id}`);
            docRef.onSnapshot((doc) => {
                this.zone.run(() => {
                    this.genre = <Genre>doc.data();
                    subscriber.next(this.genre);
                });
            });
        });
    }
    
}
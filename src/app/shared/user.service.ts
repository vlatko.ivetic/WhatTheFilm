import { Injectable } from "@angular/core";

const firebase = require("nativescript-plugin-firebase");

export interface User {
    id : number,
    first_name : string,
    last_name : string,
    email : string,
    liked_movies : [],
    reccomended_movies : [],
    profile_pic : string
}

@Injectable({
    providedIn: "root"
})
export class UserService {

    getUserInfo() {
        
    }
}
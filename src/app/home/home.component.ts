import { Component, OnInit } from "@angular/core";
import { Movie, MovieService } from "../shared/movie.service";
import { Genre, GenreService } from "../shared/genre.service";

@Component({
    selector: "Home",
    moduleId: module.id,
    templateUrl: "./home.component.html",
    providers: [MovieService, GenreService],
})

export class HomeComponent implements OnInit {
    movies: Array<Movie> = [];
    //moviesGenre : Array<Movie> = [];
    genres: Array<Genre> = [];
    moviesLoaded = false;
    constructor(private _movieService: MovieService, private _genreService: GenreService) { }

    ngOnInit(): void {
        this._genreService.getGenres().subscribe(loadedGenres => {
            loadedGenres.forEach((genreObject) => {
                this.genres.unshift(genreObject);
            });
            //console.log(this.genres[0].name);
        });
        this._movieService.getMovies().subscribe(loadedMovies => {
            loadedMovies.forEach((movieObject) => {
                this.movies.unshift(movieObject);
            });   
            this.moviesLoaded = true;
            //console.log(this.movies[0].original_title);
        });
    }
}

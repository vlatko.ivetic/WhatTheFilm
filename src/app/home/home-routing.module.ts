import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { HomeComponent } from "./home.component";
import { MovieDetailComponent } from "./movie-detail/movie-detail.component";

const routes: Routes = [
    { path: "default", component: HomeComponent },
    { path: "movie/:id", component: MovieDetailComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class HomeRoutingModule { }

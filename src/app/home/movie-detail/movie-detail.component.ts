import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { RouterExtensions } from "nativescript-angular/router";
import { Movie, MovieService } from "../../shared/movie.service";
import { Genre, GenreService } from "../../shared/genre.service";
import { Emotion, EmotionService } from "~/app/shared/emotion.service";

@Component({
    selector: "MovieDetail",
    moduleId: module.id,
    templateUrl: "./movie-detail.component.html",
    providers: [MovieService, GenreService],
    styleUrls: ["./movie-detail.component.scss"]
})
export class MovieDetailComponent implements OnInit {
    movie: Movie;
    genre : Genre;
    genres: Array<Genre> = [];
    similarMovies: Array<Movie> = [];
    similarLoaded = true;
    movieLoaded = false;
    emotions: Array<Emotion> = []

    constructor(
        private _movieService: MovieService,
        private _genreService: GenreService,
        private _route: ActivatedRoute,
        private _routerExtensions: RouterExtensions,
        private _emotionService : EmotionService
    ) { }

    ngOnInit(): void {
        this._genreService.getGenres().subscribe(loadedGenres => {
            loadedGenres.forEach((genreObject) => {
                this.genres.unshift(genreObject);
            });
            //console.log(this.genres[0].name);
        });
        const id = +this._route.snapshot.params.id;
        this._movieService.getMovieByID(id).subscribe(loadedMovie => {
            this.movie = loadedMovie
            this.movieLoaded = true;
            //console.log(this.movie.original_title);
        });
        this._movieService.getSimilarMovies(id).subscribe(loadedMovies => {
            loadedMovies.forEach((movieObject) => {
                this.similarMovies.unshift(movieObject);
            });   
            this.similarLoaded = true;
            console.log("da")
            console.log(this.similarMovies[0].original_title)
        });
        this._emotionService.getEmotions().subscribe(loadedEmotions => {
            loadedEmotions.forEach((emotion) => {
                this.emotions.unshift(emotion);
            });
            console.log(this.emotions[0].unicode)
        })
        
    }

    onBackTap(): void {
        this._routerExtensions.back();
    }
}

import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { RouterExtensions } from "nativescript-angular/router";
import { Movie, MovieService } from "./../shared/movie.service";
import { Genre, GenreService } from "./../shared/genre.service";
import { ItemEventData } from "tns-core-modules/ui/list-view/list-view";


@Component({
    selector: "MoviesGenre",
    moduleId: module.id,
    templateUrl: "./movies-genre.component.html",
    providers: [MovieService, GenreService],
})
export class MoviesGenreComponent implements OnInit {

    selectedGenre : Genre;  
    movies: Array<Movie> = [];
    genres: Array<Genre> = [];
    public selectedMovie;

    constructor(
        private _movieService: MovieService, 
        private _genreService: GenreService,
        private _route: ActivatedRoute,
        private _routerExtensions: RouterExtensions
    ) { }

    ngOnInit(): void {

        const id = +this._route.snapshot.params.id;
        this._genreService.getGenreByID(id).subscribe(loadedGenre => {
            this.selectedGenre = loadedGenre
            //console.log(this.selectedGenre.name)
        })

        this._genreService.getGenres().subscribe(loadedGenres => {
            loadedGenres.forEach((genreObject) => {
                this.genres.unshift(genreObject);
            });
            //console.log(this.genres[0].name);
        });

        this._movieService.getMoviesByGenre(id).subscribe( moviesGenre => {
            moviesGenre.forEach((movie) => {
                this.movies.unshift(movie);
            });
            //console.log(this.movies[0].original_title)
      })

    }

    onItemTap(args: ItemEventData): void {
        this.selectedMovie = this.movies[args.index].original_title;
        console.log(this.selectedMovie)
    }

    onBackTap(): void {
        this._routerExtensions.back();
    }
}
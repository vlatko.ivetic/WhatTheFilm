import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { MoviesGenreComponent } from "./movies-genre.component";

import { MovieDetailComponent } from "../home/movie-detail/movie-detail.component";

const routes: Routes = [
    { path: "default", component: MoviesGenreComponent },
    { path: "movie/:id", component: MovieDetailComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class HomeRoutingModule { }